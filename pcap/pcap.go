package pcap

import (
	"errors"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"os"
)

const (
	Err_open string = "Not Open"
)

type PCAPReader struct {
	Handle *pcap.Handle
	Source *gopacket.PacketSource
	Opened error
	File   string
}

func NewPCAPReader(file string) *PCAPReader {
	return &PCAPReader{
		Opened: errors.New(Err_open),
		File:   file,
	}
}

func (p *PCAPReader) Open() error {
	_, p.Opened = os.Stat(p.File)
	if p.Opened != nil {
		return p.Opened
	}

	p.Handle, p.Opened = pcap.OpenOffline(p.File)
	p.Source = gopacket.NewPacketSource(p.Handle, p.Handle.LinkType())
	return p.Opened
}

func (p *PCAPReader) GetNext() (gopacket.Packet, error) {
	return p.Source.NextPacket()
}

func (p *PCAPReader) Packets() chan gopacket.Packet {
	return p.Source.Packets()
}
