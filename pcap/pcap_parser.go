package pcap

import ()

type PacketParser struct {
	Deliver *PCAPReader
}

func NewPacketParser(deliver *PCAPReader) (*PacketParser, error) {
	p := &PacketParser{
		Deliver: deliver,
	}

	if p.Deliver.Opened != nil {
		if p.Deliver.Opened.Error() == Err_open {
			p.Deliver.Opened = p.Deliver.Open()
		}

		if p.Deliver.Opened != nil {
			return nil, p.Deliver.Opened
		}
	}

	return p, nil
}
