package main

import (
	"NetMap/pcap"
	"flag"
	log "github.com/sirupsen/logrus"
)

var (
	flags Flag = Flag{}
)

type Flag struct {
	Pcap string
	Host string
	Port string
}

func init() {
	flag.StringVar(&flags.Pcap, "c,cap", "./capture.pcap", "The Capture File")
	flag.StringVar(&flags.Host, "h,host", "localhost", "The bolt cypher QL host")
	flag.StringVar(&flags.Port, "p,port", "7687", "The bolt cypher QL port")
	flag.Parse()
}

func main() {
	log.Infof("Reading packets from %s", flags.Pcap)

	deliver := pcap.NewPCAPReader(flags.Pcap)

	err := deliver.Open()
	if err != nil {
		log.Errorf("Could not open pcap %s", err)
		return
	}
}
